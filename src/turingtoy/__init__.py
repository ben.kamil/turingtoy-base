from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)



# class TuringMachine:
#     def __init__(self, machine: Dict, steps: Optional[int] = None) -> None:
#         self.blank: str = machine['blank']
#         self.current_state: Dict | str = machine['start state']
#         self.final_states: List[str] = machine['final states']
#         self.table: Dict = machine['table']
#         self.tape: List[str] = []
#         self.history: List[Dict] = []
#         self.steps = steps

#     def load_tape(self, tape_input: str) -> None:
#         self.tape = list(tape_input)
#         self.head_position: int = 0

#     def step(self) -> None:

#         if self.head_position >= len(self.tape):
#             self.tape.append(self.blank)
#         elif self.head_position < 0:
#             self.tape.insert(0, self.blank)
#             head_position = 0
#         symbol : str = self.tape[self.head_position]
#         if self.current_state in self.table and symbol in self.table[self.current_state]:
#             transition: Dict | str = self.table[self.current_state][symbol]
#             current_history : Dict = {"state": self.current_state, "reading": symbol, "position": self.head_position, "memory": "".join(self.tape), "transition": transition}
#             self.history.append(current_history)
#             if type(transition) == str:
#                 if transition == "R":
#                     head_position += 1
#                 else :
#                     head_position -= 1
#             else:
#                 for rule in transition:
#                     val: str = transition[rule]
#                     if rule == "R":
#                         current_state = val
#                         head_position += 1
#                     elif rule == "L":
#                         current_state = val
#                         head_position -= 1
#                     else:
#                         self.tape[head_position] = val
#                 # if val == "done":
#                 #     break

#     def run(self) -> None:
#         while self.current_state not in self.final_states:
#             if self.steps is not None:
#                 if self.steps <= 0:
#                     break
#                 self.steps -= 1
#             self.step()


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    blank: str = machine.get('blank')
    table: Dict = machine.get('table')
    current_state: Dict | str = machine.get('start state')
    tape: List[str] = list(input_)
    head_position: int = 0
    history: List[Dict] = []

    # turing_machine = TuringMachine(machine)
    # turing_machine.load_tape(input_)

    while True:
        if steps is not None:
            if steps <= 0:
                break
            steps -= 1

        if head_position >= len(tape):
            tape.append(blank)
        elif head_position < 0:
            tape.insert(0, blank)
            head_position = 0
        symbol = tape[head_position]
        transition: Dict | str = table[current_state][symbol]
        current_history : Dict = {"state": current_state, "reading": symbol, "position": head_position, "memory": "".join(tape), "transition": transition}
        history.append(current_history)

        if type(transition) == str:
            if transition == "R":
                head_position += 1
            else :
                head_position -= 1
        else:
            for rule in transition:
                val: str = transition[rule]
                if rule == "R":
                    current_state = val
                    head_position += 1
                elif rule == "L":
                    current_state = val
                    head_position -= 1
                else:
                    tape[head_position] = val
            if val == "done":
                break

    output = "".join(tape).strip(blank)
    return output, history, True

